import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Request e2e test', () => {

    let navBarPage: NavBarPage;
    let requestDialogPage: RequestDialogPage;
    let requestComponentsPage: RequestComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Requests', () => {
        navBarPage.goToEntity('request');
        requestComponentsPage = new RequestComponentsPage();
        expect(requestComponentsPage.getTitle()).toMatch(/flightApp.request.home.title/);

    });

    it('should load create Request dialog', () => {
        requestComponentsPage.clickOnCreateButton();
        requestDialogPage = new RequestDialogPage();
        expect(requestDialogPage.getModalTitle()).toMatch(/flightApp.request.home.createOrEditLabel/);
        requestDialogPage.close();
    });

    it('should create and save Requests', () => {
        requestComponentsPage.clickOnCreateButton();
        requestDialogPage.setNameInput('name');
        expect(requestDialogPage.getNameInput()).toMatch('name');
        requestDialogPage.setTraveldateInput(12310020012301);
        expect(requestDialogPage.getTraveldateInput()).toMatch('2001-12-31T02:30');
        requestDialogPage.setDestinationInput('destination');
        expect(requestDialogPage.getDestinationInput()).toMatch('destination');
        requestDialogPage.setStateInput('state');
        expect(requestDialogPage.getStateInput()).toMatch('state');
        requestDialogPage.setRegistrationdateInput(12310020012301);
        expect(requestDialogPage.getRegistrationdateInput()).toMatch('2001-12-31T02:30');
        requestDialogPage.save();
        expect(requestDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RequestComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-request div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RequestDialogPage {
    modalTitle = element(by.css('h4#myRequestLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    traveldateInput = element(by.css('input#field_traveldate'));
    destinationInput = element(by.css('input#field_destination'));
    stateInput = element(by.css('input#field_state'));
    registrationdateInput = element(by.css('input#field_registrationdate'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setTraveldateInput = function (traveldate) {
        this.traveldateInput.sendKeys(traveldate);
    }

    getTraveldateInput = function () {
        return this.traveldateInput.getAttribute('value');
    }

    setDestinationInput = function (destination) {
        this.destinationInput.sendKeys(destination);
    }

    getDestinationInput = function () {
        return this.destinationInput.getAttribute('value');
    }

    setStateInput = function (state) {
        this.stateInput.sendKeys(state);
    }

    getStateInput = function () {
        return this.stateInput.getAttribute('value');
    }

    setRegistrationdateInput = function (registrationdate) {
        this.registrationdateInput.sendKeys(registrationdate);
    }

    getRegistrationdateInput = function () {
        return this.registrationdateInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
