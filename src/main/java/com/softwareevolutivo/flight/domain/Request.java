package com.softwareevolutivo.flight.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;


/**
 * A Request.
 */
@Entity
@Table(name = "request")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Request implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "traveldate")
    private Instant traveldate;

    @Column(name = "destination")
    private String destination;

    @Column(name = "state")
    private String state;

    @Column(name = "registrationdate")
    private Instant registrationdate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Request name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getTraveldate() {
        return traveldate;
    }

    public Request traveldate(Instant traveldate) {
        this.traveldate = traveldate;
        return this;
    }

    public void setTraveldate(Instant traveldate) {
        this.traveldate = traveldate;
    }

    public String getDestination() {
        return destination;
    }

    public Request destination(String destination) {
        this.destination = destination;
        return this;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getState() {
        return state;
    }

    public Request state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Instant getRegistrationdate() {
        return registrationdate;
    }

    public Request registrationdate(Instant registrationdate) {
        this.registrationdate = registrationdate;
        return this;
    }

    public void setRegistrationdate(Instant registrationdate) {
        this.registrationdate = registrationdate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Request request = (Request) o;
        if (request.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), request.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Request{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", traveldate='" + getTraveldate() + "'" +
            ", destination='" + getDestination() + "'" +
            ", state='" + getState() + "'" +
            ", registrationdate='" + getRegistrationdate() + "'" +
            "}";
    }
}
