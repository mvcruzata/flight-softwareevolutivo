/**
 * View Models used by Spring MVC REST controllers.
 */
package com.softwareevolutivo.flight.web.rest.vm;
