import { BaseEntity } from './../../shared';

export class Request implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public traveldate?: any,
        public destination?: string,
        public state?: string,
        public registrationdate?: any,
    ) {
    }
}
